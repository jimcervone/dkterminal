# Wizardry.sh
# Git and Terminal Helper Tools
# Created By Jim Cervone on January 20, 2017



alias edit='open'				# correct way is to set your $EDITOR environment var
alias bp='edit ~/.bash_profile' # edits the .bash_profile file with your set editor
alias please="sudo"             # "SuperUser Do" administrative command - requires password
alias f='open -a Finder ./'     # opens the current directory in macOS Finder
alias ll='ls -FG1Ahp'			# detailed directory listing



# pipe, grep, and cat into a new file

fileCalled() {
	ls | grep $1
}

findInDir() {
	find . | grep $1
}

duplicate() {
	cat $1 > "dup-$1"
}



# Git Shortcuts

alias gs='git status'

gb() {
	if [ -z "$1" ]; then # -z passes if the argument doesn't exist
		git branch
	else
		if [ -z $(git branch | grep "$1") ]; then
			git checkout -b $1
		else
			git checkout $1
		fi
	fi
}

gitRemoveMergedBranches() {
	# thanks, Internet!
	git branch --merged | grep -v "\*" | grep -v master | grep -v dev | xargs -n 1 git branch -d
	# ...but be CAREFUL!
}



# Add Some Flavor When Your Terminal Opens (and your .bash_profile executes)

echo;
tput setaf 2
echo "############# DIAMOND KINETICS #############"
tput sgr0
echo "######## ENGINEERING BETTER PLAYERS ########"



# This is ...fun

weather() {
	curl -s wttr.in/pittsburgh
}



# New PS1 (Like Mine!)

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

jimsPS1() {
	source $DIR/.git-prompt.sh # copied from the git source. enables $(__git_ps1)
	source $DIR/.git-completion.bash # enables tab completion within git

	GIT_PS1_SHOWDIRTYSTATE=true
	GIT_PS1_DESCRIBE_STYLE="branch"

	GREEN="\[$(tput setaf 2)\]"
	YELLOW="\[$(tput setaf 3)\]"
	CYAN="\[$(tput setaf 6)\]"
	RESET="\[$(tput sgr0)\]"

	export PS1="${GREEN}\n=================[\u @ \h]=====================${RESET}\n\t ${CYAN}\w${YELLOW}\$(__git_ps1)${RESET}\n\$ "
}

# uncomment this line to use this as your default PS1
# jimsPS1;



# Reset It All

restoreSystemDefaultPS1() {
	source ~/.git-prompt.sh
	GIT_PS1_SHOWDIRTYSTATE=false

	export PS1="\h:\W \u\$ "
}

alias dammit-jim="restoreSystemDefaultPS1"
